﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyDisplay : MonoBehaviour {

    public SO_Enemy enemy_so;
    public Enemy enemy_scr;

    public string enemyname;

    public SpriteRenderer icon;

	void Awake () {
        enemy_scr = GetComponent<Enemy>();
        enemyname = enemy_so.name;
        enemy_scr.startspeed = enemy_so.mov_speed;
        enemy_scr.starthealth = enemy_so.health;
    
        icon.sprite = enemy_so.Icon;
    }
}
